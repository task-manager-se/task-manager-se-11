package ru.zolov.tm.command.task;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.Task;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public final class TaskRemoveCommand extends AbstractCommand {

  private final String name = "task-remove";
  private final String description = "Remove task";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws EmptyStringException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    @NotNull final String userName = TerminalUtil.nextLine();
    @NotNull final List<Task> listOfTasks = bootstrap.getTaskEndpoint().findAllTasksByUserId(session);
    for (Task task : listOfTasks) {
      System.out.println("________________________________________");
      System.out.println(String.format(
          "%n Task: %s " + "%n Task ID: %s " + "%n Task description: %s " + "%n Status: %s " + "%n Date of create: %s "
              + "%n Date of start: %s " + "%n Date of finish: %s", task.getName(), task.getId(), task.getDescription(), task
              .getStatus(), task.getDateOfCreate(), task.getDateOfStart(), task.getDateOfFinish()));
    }
    System.out.print("Enter task id: ");
    final String taskId = TerminalUtil.nextLine();
    System.out.println(bootstrap.getTaskEndpoint().removeOneTaskById(session, taskId));
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
