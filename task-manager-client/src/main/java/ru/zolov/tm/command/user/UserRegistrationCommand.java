package ru.zolov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.User;
import ru.zolov.tm.api.UserExistException_Exception;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public class UserRegistrationCommand extends AbstractCommand {

  private final String name = "user-registration";
  private final String description = "New user registration";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return true;
  }

  @Override public void execute() throws UserExistException_Exception, EmptyStringException_Exception, EmptyRepositoryException_Exception {
    System.out.println("Enter login : ");
    @NotNull final String login = TerminalUtil.nextLine();
    System.out.println("Enter password : ");
    @NotNull String password = TerminalUtil.nextLine();
    @NotNull final User user = bootstrap.getUserEndpoint().registerNewUser(login, password);
    System.out.println("User " + user.getName() + " successfully added!");
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
