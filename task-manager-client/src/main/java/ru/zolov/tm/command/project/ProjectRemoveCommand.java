package ru.zolov.tm.command.project;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.Project;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;


public final class ProjectRemoveCommand extends AbstractCommand {

  private final String name = "project-remove";
  private final String description = "Remove project";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws EmptyStringException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    @NotNull final List<Project> list = bootstrap.getProjectEndpoint().findAllProject(session);
    final List<Project> listOfProjects = bootstrap.getProjectEndpoint().findAllProject(session);
    for (Project project : listOfProjects) {
      System.out.println("________________________________________");
      System.out.println(String.format(
          "%n Project: %s " + "%n Project ID: %s " + "%n Project description: %s " + "%n Status: %s " + "%n Date of create: %s "
              + "%n Date of start: %s " + "%n Date of finish: %s", project.getName(), project.getId(), project.getDescription(), project
              .getStatus(), project.getDateOfCreate(), project.getDateOfStart(), project.getDateOfFinish()));
    }

    System.out.print("Enter project id: ");
    @NotNull final String projectId = TerminalUtil.nextLine();
    System.out.println(bootstrap.getProjectEndpoint().removeProjectById(session, projectId));
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
