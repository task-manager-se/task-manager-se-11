package ru.zolov.tm.command.data.xml;

import javax.xml.bind.JAXBException;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.JAXBException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;

public class DataXmlJaxBLoadCommand extends AbstractCommand {

  private final String name = "dataload-xml-jaxb";
  private final String description = "Load data from xml file (jaxb)";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return true;
  }

  @Override public void execute() throws JAXBException, AccessForbiddenException_Exception, EmptyStringException_Exception, JAXBException_Exception, CloneNotSupportedException_Exception, EmptyRepositoryException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    final boolean check = (session == null || session.getRoleType() != RoleType.ADMIN);
    if (check) throw new AccessForbiddenException_Exception();
    bootstrap.getDomainEndpoint().loadFromXmlJaxb(session);
    System.out.println("Data loaded!");
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN};
  }
}
