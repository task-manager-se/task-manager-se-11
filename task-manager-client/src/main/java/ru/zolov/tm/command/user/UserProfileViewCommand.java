package ru.zolov.tm.command.user;

import java.util.Date;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.UserNotFoundException_Exception;
import ru.zolov.tm.command.AbstractCommand;

public class UserProfileViewCommand extends AbstractCommand {

  private final String name = "user-profile";
  private final String description = "View user profile";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws EmptyStringException_Exception, UserNotFoundException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) throw new AccessForbiddenException_Exception();
    Date date = new Date(session.getTimestamp());
    System.out.println(" UserID: " + session.getUserId() + "%n UserRole: " + session.getRoleType() + "%n Date of create: " + date);

  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
