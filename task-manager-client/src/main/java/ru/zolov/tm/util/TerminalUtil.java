package ru.zolov.tm.util;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Scanner;


public class TerminalUtil {
  //  private static final Map<String, Comparator<AbstractGoal>> comparators = new HashMap<>();
  //  private static final Comparator<AbstractGoal> dateCreateComparator = new DateCreateComparator();
  //  private static final Comparator<AbstractGoal> dateStartComparator = new DateStartComparator();
  //  private static final Comparator<AbstractGoal> dateFinishComparator = new DateFinishComparator();
  //  private static final Comparator<AbstractGoal> statusComparator = new StatusComparator();

  private static final Scanner scanner = new Scanner(System.in);
  public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);

  public static String nextLine() {
    return scanner.nextLine();
  }

  public static Integer nextInt() {
    return scanner.nextInt();
  }

  //  public static void performGoal(
  //      @Nullable final AbstractGoal goal,
  //      @Nullable final String userName)
  //      throws
  //      EmptyRepositoryException,
  //      EmptyStringException {
  //    if (goal == null) throw new EmptyRepositoryException();
  //    if (userName == null || userName.isEmpty()) throw new EmptyStringException();
  //    final String name = goal.getName();
  //    final String status = goal.getDescription();
  //    final String dateOfCreate = performDate(goal.getDateOfCreate());
  //    final String dateOfStart = performDate(goal.getDateOfStart());
  //    final String dateOfFinish = performDate(goal.getDateOfFinish());
  //    final String id = goal.getId();
  //    System.out
  //        .printf("%n Name: %s %n"
  //            + " Creator: %s %n "
  //            + "Status: %s %n"
  //            + " Date of create: %s %n"
  //            + " Date of start: %s %n"
  //            + " Date of finish: %s %n"
  //            + " ID: %s %n", name, userName, status, dateOfCreate, dateOfStart, dateOfFinish, id);
  //  }

  //  public static void performList(
  //      @NotNull final List<? extends AbstractGoal> list,
  //      @NotNull final String userName)
  //      throws
  //      EmptyRepositoryException,
  //      EmptyStringException {
  //    int position = 1;
  //    for (AbstractGoal goal : list) {
  //      System.out.print(position + ": ");
  //      performGoal(goal, userName);
  //      position++;
  //    }
  //  }

  //  public static String performDate(Date date) {
  //    if (date == null) return "Date has not been set";
  //    return dateFormat.format(date);
  //  }
}
