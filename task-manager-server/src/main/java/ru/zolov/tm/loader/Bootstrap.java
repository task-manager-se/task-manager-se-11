package ru.zolov.tm.loader;

import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ISessionRepository;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.ITerminalService;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.endpoint.AbstractEndpoint;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.StatusType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.SessionRepository;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.service.DomainService;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.SessionService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;
import ru.zolov.tm.service.UserService;
import ru.zolov.tm.util.PublisherUtil;

public final class Bootstrap implements ServiceLocator {

  private final IProjectRepository projectRepository = new ProjectRepository();
  private final ITaskRepository taskRepository = new TaskRepository();
  private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);
  private final ITaskService taskService = new TaskService(taskRepository);
  private final ITerminalService terminalService = new TerminalService();
  private final IUserRepository userRepository = new UserRepository();
  private final IUserService userService = new UserService(userRepository);
  private final IDomainService domainService = new DomainService(this);
  private final ISessionRepository sessionRepository = new SessionRepository();
  private final ISessionService sessionService = new SessionService(sessionRepository);
  private final Set<AbstractEndpoint> endpoits = new LinkedHashSet<>();
  private final Set<Class<? extends AbstractEndpoint>> endpointClasses = new Reflections("ru.zolov.tm.endpoint")
      .getSubTypesOf(AbstractEndpoint.class);

  @NotNull @Override public IProjectService getProjectService() {
    return projectService;
  }

  @NotNull @Override public ITaskService getTaskService() {
    return taskService;
  }

  @NotNull @Override public ITerminalService getTerminalService() {
    return terminalService;
  }

  @NotNull @Override public IUserService getUserService() {
    return userService;
  }

  @NotNull @Override public IDomainService getDomainService() {
    return domainService;
  }

  @Override public @NotNull ISessionService getSessionService() {
    return sessionService;
  }

  public void registryEndpoint(
      @NotNull final Set<Class<? extends AbstractEndpoint>> classes
  ) throws Exception {
    for (@NotNull final Class clazz : classes) {
      registryEndpoint(clazz);
    }
  }

  public void registryEndpoint(
      @NotNull final Class clazz
  ) throws Exception {
    if (!AbstractEndpoint.class.isAssignableFrom(clazz)) return;
    Object endpoint = clazz.getDeclaredConstructor().newInstance();
    AbstractEndpoint abstractEndpoint = (AbstractEndpoint)endpoint;
    registryEndoint(abstractEndpoint);
  }

  public void registryEndoint(AbstractEndpoint endpoint) throws EmptyRepositoryException {
    if (endpoint == null) throw new EmptyRepositoryException();
    endpoint.setServiceLocator(this);
    endpoits.add(endpoint);
  }

  public void init() {
    try {
      BasicConfigurator.configure();
      registryEndpoint(endpointClasses);
      PublisherUtil.publish(endpoits);
      initEntities();
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  public void initEntities() throws EmptyStringException, UserExistException, UserNotFoundException, ParseException, EmptyRepositoryException {
    userService.adminRegistration("admin", "admin");
    User admin = userService.findByLogin("admin");
    Project project1 = projectService.create(admin.getId(), "Google project", "Doing some good", "10.10.2010", "11.11.2011");
    Project project2 = projectService.create(admin.getId(), "Yandex project", "Doing some xopowo", "05.05.2010", "11.11.2011");
    Project project3 = projectService.create(admin.getId(), "Mail project", "Doing some ploho", "03.03.2010", "11.11.2011");
    project3.setStatus(StatusType.INPROGRESS);
    Project project4 = projectService.create(admin.getId(), "Amazon project", "Doing some dorogo", "01.10.2010", "11.11.2011");
    project4.setStatus(StatusType.INPROGRESS);
    Project project5 = projectService.create(admin.getId(), "Microsoft project", "Doing some neponyatno", "10.01.2010", "11.11.2011");
    project5.setStatus(StatusType.DONE);
    Task task = taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "13.10.2010", "15.10.2010");
    task.setStatus(StatusType.INPROGRESS);
    Task task1 = taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "10.10.2010", "15.10.2010");
    task1.setStatus(StatusType.INPROGRESS);
    Task task2 = taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "12.10.2010", "15.10.2010");
    task2.setStatus(StatusType.INPROGRESS);
    Task task3 = taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "15.10.2010", "21.10.2010");
    task3.setStatus(StatusType.INPROGRESS);
    taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "16.10.2010", "22.10.2010");
    taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "17.10.2010", "23.10.2010");
    taskService.create(admin.getId(), project1.getId(), "Task in google", "hard task", "18.10.2010", "24.10.2010");
    taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "14.10.2010", "15.10.2010");
    taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "12.10.2010", "15.10.2010");
    taskService.create(admin.getId(), project2.getId(), "Task in Yandex", "hard task", "16.10.2010", "15.10.2010");
  }
}