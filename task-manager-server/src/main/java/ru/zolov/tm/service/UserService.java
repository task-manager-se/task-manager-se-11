package ru.zolov.tm.service;

import java.util.Arrays;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

  private IUserRepository userRepository;
  private User currentUser = null;

  public UserService(IUserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public User getCurrentUser() {
    return currentUser;
  }

  @Override
  public void setCurrentUser(final User currentUser) {
    this.currentUser = currentUser;
  }

  @Override
  public boolean isAuth() {
    return currentUser != null;
  }

  @Override
  public User login(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable final User user = userRepository.findByLogin(login);
    if (user == null) throw new UserNotFoundException();
    final String passwordHash = HashUtil.md5(password);
    if (passwordHash == null) throw new UserNotFoundException();
    if (passwordHash.equals(user.getPasswordHash())) return user;
    else return null;
  }

  @NotNull
  @Override
  public User userRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserExistException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    if (userRepository.findByLogin(login) != null) throw new UserExistException();
    final User newUser = new User();
    newUser.setLogin(login);
    newUser.setRole(RoleType.USER);
    String hashdPassword = HashUtil.md5(password);
    if (hashdPassword == null) throw new EmptyStringException();
    newUser.setPasswordHash(hashdPassword);
    userRepository.persist(newUser);
    return newUser;
  }

  @Override
  public void adminRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    if (userRepository.findByLogin(login) != null) throw new EmptyStringException();
    @Nullable final User admin = new User();
    admin.setLogin(login);
    admin.setRole(RoleType.ADMIN);
    admin.setPasswordHash(HashUtil.md5(password));
    userRepository.persist(admin);
  }

  @Override
  public void logOut() {
    currentUser = null;
  }

  @Override
  public boolean isRolesAllowed(@Nullable final RoleType... roleTypes) {
    if (roleTypes == null) return false;
    if (currentUser == null) return false;
    final List<RoleType> types = Arrays.asList(roleTypes);
    return types.contains(currentUser.getRole());
  }

  @Override
  public User findByLogin(@Nullable final String login) throws EmptyStringException, UserNotFoundException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    @Nullable final User user = userRepository.findByLogin(login);
    if (user == null) throw new UserNotFoundException();
    return user;
  }

  @Override
  public User findById(@Nullable final String id) throws EmptyStringException, UserNotFoundException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @Nullable final User user = userRepository.findOne(id);
    if (user == null) throw new UserNotFoundException();
    return user;
  }

  @NotNull
  public List<User> readAll() throws EmptyRepositoryException {
    return userRepository.findAll();
  }

  @Override
  public boolean remove(@Nullable final String id) throws EmptyStringException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    userRepository.remove(id);
    return userRepository.remove(id);
  }

  @NotNull
  @Override
  public User updateUserPassword(
      @Nullable final String id,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable final User user = userRepository.findOne(id);
    if (user == null) throw new UserNotFoundException();
    user.setPasswordHash(HashUtil.md5(password));
    userRepository.merge(user);
    return user;
  }

  @NotNull
  @Override
  public User updateUserProfile(
      @Nullable String id,
      @Nullable String name,
      @Nullable String password
  ) throws EmptyStringException, UserNotFoundException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable final User user = userRepository.findOne(id);
    if (user == null) throw new UserNotFoundException();
    user.setLogin(name);
    user.setPasswordHash(HashUtil.md5(password));
    userRepository.merge(user);
    return user;
  }

  @Override
  public void load(@Nullable List<User> list) throws EmptyRepositoryException {
    if (list != null) userRepository.load(list);
  }
}
