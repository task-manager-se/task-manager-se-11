package ru.zolov.tm.service;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService {

  private final String NAME = "application.properties";

  private final Properties properties = new Properties();

  public void init() {
    final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
  }

  public String getServerHost() {
    final String propertyHost = properties.getProperty("server.host");
    final String envHost = System.getProperty("server.host");
    if (envHost != null) return envHost;
    return propertyHost;
  }

  public Integer getServerPort() {
    final String propertyPort = properties.getProperty("server.host");
    final String envPort = System.getProperty("server.host");
    String value = propertyPort;
    if (envPort != null) value = envPort;
    return Integer.parseInt(value);
  }
}
