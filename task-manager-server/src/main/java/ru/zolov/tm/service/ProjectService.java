package ru.zolov.tm.service;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class ProjectService extends AbstractService<Project> implements IProjectService {

  private final IProjectRepository projectRepository;
  private final ITaskRepository taskRepository;

  public ProjectService(
      IProjectRepository projectRepository,
      ITaskRepository taskRepository
  ) {
    this.projectRepository = projectRepository;
    this.taskRepository = taskRepository;
  }

  @Override public IProjectRepository getProjectRepository() {
    return projectRepository;
  }

  @Override public ITaskRepository getTaskRepository() {
    return taskRepository;
  }

  @NotNull @Override public Project create(
      @Nullable final String userId,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, ParseException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (description == null || description.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    final Project project = new Project();
    project.setName(name);
    project.setUserId(userId);
    project.setDescription(description);
    project.setDateOfStart(dateFormat.parse(start));
    project.setDateOfFinish(dateFormat.parse(finish));
    projectRepository.persist(project);
    return project;
  }

  @NotNull @Override public Project read(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (projectRepository.findOne(userId, id) == null) throw new EmptyRepositoryException();
    return projectRepository.findOne(userId, id);
  }

  @NotNull @Override public List<Project> readAll(@Nullable final String userId) throws EmptyStringException, EmptyRepositoryException {
    if (userId == null) throw new EmptyStringException();
    return projectRepository.findAll(userId);
  }

  @NotNull @Override public List<Project> readAll() throws EmptyRepositoryException {
    return projectRepository.findAll();
  }

  @Override public void update(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable String start,
      @Nullable String finish
  ) throws ParseException {
    if (userId == null || userId.isEmpty()) return;
    if (id == null || id.isEmpty()) return;
    if (name == null || name.isEmpty()) return;
    final Date startDate = dateFormat.parse(start);
    final Date finishDate = dateFormat.parse(finish);
    projectRepository.update(userId, id, name, description, startDate, finishDate);
  }

  @Override public boolean remove(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException {
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    return projectRepository.remove(userId, id);
  }

  @NotNull public List<Project> sortBy(
      @Nullable final String userId,
      Comparator<AbstractGoal> comparator
  ) throws EmptyRepositoryException {
    final List<Project> list = projectRepository.findAll(userId);
    Collections.sort(list, comparator);
    return list;
  }

  @NotNull @Override public List<Project> findProject(
      @Nullable final String userId,
      @Nullable final String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException {
    if (userId == null || userId.isEmpty()) {
      throw new EmptyStringException();
    }
    if (partOfTheName == null) throw new EmptyStringException();
    if (partOfTheName.isEmpty()) return projectRepository.findAll(userId);
    final List<Project> list = projectRepository.findProject(userId, partOfTheName);
    if (list == null) throw new EmptyRepositoryException();
    return list;
  }

  public void load(@Nullable List<Project> list) throws EmptyRepositoryException {
    if (list != null) projectRepository.load(list);
  }
}
