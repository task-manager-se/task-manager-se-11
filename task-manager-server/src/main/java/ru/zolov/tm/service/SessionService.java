package ru.zolov.tm.service;


import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.ISessionRepository;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.util.SessionUtil;

public class SessionService extends AbstractService<Session> implements ISessionService {

  private ISessionRepository sessionRepository;

  public SessionService(final ISessionRepository sessionRepository) {
    this.sessionRepository = sessionRepository;
  }

  public static boolean isALive(@NotNull final Long sessionTimeStamp) {
    final long sessionLiveTimeMs = 4400000L;
    final long currentTimeMs = System.currentTimeMillis();
    return ((currentTimeMs - sessionTimeStamp) <= sessionLiveTimeMs);
  }

  @NotNull @Override public Session open(final User user) {
    final Session session = new Session();
    session.setUserId(user.getId());
    session.setRoleType(user.getRole());
    session.setSignature(SessionUtil.sign(session));
    sessionRepository.persist(session);
    return session;
  }

  @Override public void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException {
    if (session == null) throw new AccessForbiddenException();
    if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
    if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
    final Session temp = session.clone();
    if (temp == null) throw new AccessForbiddenException();
    final String signatureSource = session.getSignature();
    temp.setSignature(null);
    final String signatureTarget = SessionUtil.sign(temp);
    final boolean check = signatureSource.equals(signatureTarget);
    final boolean fresh = isALive(session.getTimestamp());
    if (!check && fresh) throw new AccessForbiddenException();
    if (sessionRepository.findOne(session.getId()) == null) throw new AccessForbiddenException();
  }

  @Override public void close(Session session) {
    sessionRepository.remove(session.getId());
  }
}
