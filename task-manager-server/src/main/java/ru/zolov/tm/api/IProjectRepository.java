package ru.zolov.tm.api;

import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectRepository extends IRepository<Project> {

  @NotNull Project findOne(
      @NotNull String userId,
      @NotNull String id
  ) throws EmptyStringException, EmptyRepositoryException;

  @NotNull List<Project> findAll(@NotNull String userId) throws EmptyRepositoryException;

  @NotNull List<Project> findAll() throws EmptyRepositoryException;

  @NotNull Project persist(@NotNull Project project);

  void merge(@NotNull Project project);

  void update(
      @NotNull String userId,
      @NotNull String id,
      @NotNull String name,
      @NotNull String description,
      @NotNull Date start,
      @NotNull Date finish
  );

  boolean remove(
      @NotNull String userId,
      @NotNull String id
  );

  void removeAll(@NotNull String userId) throws EmptyRepositoryException;

  @NotNull List<Project> findProject(
      @NotNull String userId,
      @NotNull String partOfTheName
  ) throws EmptyRepositoryException;
}
