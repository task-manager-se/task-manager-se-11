package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractEntity;
import ru.zolov.tm.exception.EmptyRepositoryException;

public interface IRepository<E extends AbstractEntity> {

  @NotNull List<E> findAll() throws EmptyRepositoryException;

  @Nullable E findOne(String id);

  E persist(@NotNull E entity);

  void merge(@NotNull E entity);

  boolean remove(@NotNull String id);

  void removeAll();

  void load(@NotNull List<E> list);
}
