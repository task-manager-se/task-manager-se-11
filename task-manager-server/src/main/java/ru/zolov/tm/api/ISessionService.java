package ru.zolov.tm.api;

import java.text.ParseException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;

public interface ISessionService {

  @NotNull Session open(User user) throws ParseException;

  void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException;

  void close(Session session);
}
