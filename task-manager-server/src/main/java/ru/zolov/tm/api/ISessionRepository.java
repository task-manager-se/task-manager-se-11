package ru.zolov.tm.api;


import ru.zolov.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

}
