package ru.zolov.tm.api;

import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskService {

  ITaskRepository getTaskRepository();

  @NotNull Task create(
      @Nullable String userId,
      @Nullable String projectId,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, EmptyRepositoryException, ParseException;

  @NotNull List<Task> readAll(@Nullable String userId) throws EmptyStringException, EmptyRepositoryException;

  @NotNull List<Task> readAll() throws EmptyStringException, EmptyRepositoryException;

  @Nullable List<Task> readTaskByProjectId(
      @Nullable String userId,
      String id
  ) throws EmptyStringException, EmptyRepositoryException;

  @Nullable Task readTaskById(
      @Nullable String userId,
      @Nullable String projectId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException;

  boolean remove(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException;

  void update(
      @Nullable String userId,
      @Nullable String id,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, EmptyRepositoryException, ParseException;

  @NotNull List<Task> sortBy(
      @Nullable String userId,
      @Nullable String projectId,
      @Nullable Comparator<AbstractGoal> comparator
  ) throws EmptyStringException, EmptyRepositoryException;

  @NotNull List<Task> findTask(
      @Nullable String userId,
      @Nullable String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException;

  void load(@Nullable final List<Task> list) throws EmptyRepositoryException;
}
