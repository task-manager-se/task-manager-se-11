package ru.zolov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lombok.Cleanup;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.enumerated.PathEnum;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements ru.zolov.tm.api.IDomainEndpoint {

  @Override @WebMethod public void saveToBin(
      @NotNull @WebParam(name = "session") final Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Save data to file.");
    @NotNull final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable final Path path = Paths.get(PathEnum.BIN.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable final File file = path.toFile();
    if (file == null) return;
    @Cleanup final FileOutputStream fos = new FileOutputStream(file);
    @Cleanup final ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(domain);
    System.out.println("DONE!");
  }

  @Override @WebMethod public void loadFromBin(
      @NotNull @WebParam(name = "session") final Session session
  ) throws IOException, ClassNotFoundException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException {
    System.out.println("Loading from file ");
    serviceLocator.getSessionService().validate(session);
    @Nullable final File file = new File(PathEnum.BIN.getPath());
    @Cleanup final FileInputStream fis = new FileInputStream(file);
    @Cleanup final ObjectInputStream ois = new ObjectInputStream(fis);
    @NotNull final Domain domain = (Domain)ois.readObject();
    serviceLocator.getDomainService().load(domain);
    System.out.println("DONE!");
  }

  @Override @WebMethod public void saveToJsonJackson(
      @NotNull @WebParam(name = "session") final Session session
  ) throws EmptyStringException, EmptyRepositoryException, IOException, AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Save data to json file (jackson)");
    @NotNull final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable final Path path = Paths.get(PathEnum.JSON.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable final File file = path.toFile();
    if (file == null) return;
    @NotNull final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
    objectWriter.writeValue(file, domain);
    System.out.println("Data saved to " + file.getPath());
  }

  @Override @WebMethod public void loadFromJsonJackson(
      @NotNull @WebParam(name = "session") final Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Load data from json file (jackson)");
    @NotNull final File file = new File(PathEnum.JSON.getPath());
    @NotNull final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable final Domain domain = objectMapper.readValue(file, Domain.class);
    if (domain == null) return;
    serviceLocator.getDomainService().load(domain);
    System.out.println("Load data from " + file.getPath());
  }

  @Override @WebMethod public void loadFromJsonJaxb(
      @NotNull @WebParam(name = "session") final Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, JAXBException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Load data from json file (jaxb)");
    @NotNull final File file = new File(PathEnum.JAXBJSON.getPath());
    Map<String, Object> properties = new HashMap<>(2);
    properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
    properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{Domain.class}, properties);
    @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    @NotNull final Domain domain = (Domain)unmarshaller.unmarshal(file);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Load data from " + file.getPath());
  }

  @Override @WebMethod public void saveToJsonJaxb(
      @NotNull @WebParam(name = "session") final Session session
  ) throws EmptyStringException, EmptyRepositoryException, IOException, JAXBException, AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Save data to json file (jaxb)");
    @NotNull final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable final Path path = Paths.get(PathEnum.JAXBJSON.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable File file = path.toFile();
    if (file == null) return;
    @NotNull final FileOutputStream fos = new FileOutputStream(file);
    @NotNull final Map<String, Object> properties = new HashMap<>();
    properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
    properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{Domain.class}, properties);
    @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.marshal(domain, fos);
    System.out.println("Data saved to " + file.getPath());
  }

  @Override @WebMethod public void saveToXmlJaxb(
      @NotNull @WebParam(name = "session") final Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, IOException, JAXBException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Save data to xml file (jaxb)");
    @NotNull final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable final Path path = Paths.get(PathEnum.XML.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable final File file = path.toFile();
    if (file == null) return;
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
    @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.marshal(domain, file);
    System.out.println("Data saved to " + file.getPath());
  }

  @Override @WebMethod public void loadFromXmlJaxb(
      @NotNull @WebParam(name = "session") final Session session
  ) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException, JAXBException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Load data from xml file (jaxb)");
    @NotNull final File file = new File(PathEnum.XML.getPath());
    @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
    @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    @NotNull final Domain domain = (Domain)unmarshaller.unmarshal(file);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Data loaded from " + file.getPath());
  }

  @Override @WebMethod public void saveToXmlJackson(
      @NotNull @WebParam(name = "session") final Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Save data to xml file (jackson)");
    @NotNull final Domain domain = new Domain();
    serviceLocator.getDomainService().save(domain);
    @Nullable final Path path = Paths.get(PathEnum.XML.getPath());
    if (path == null) return;
    if (Files.notExists(path.getParent())) Files.createDirectory(path.getParent());
    @Nullable File file = path.toFile();
    if (file == null) return;
    @NotNull final ObjectMapper objectMapper = new XmlMapper().configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
    objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    System.out.println("Data saved to " + file.getPath());
  }

  @Override @WebMethod public void loadFromXmlJackson(
      @NotNull @WebParam(name = "session") final Session session
  ) throws IOException, EmptyStringException, EmptyRepositoryException, AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    System.out.println("Load data from xml file (jackson)");
    @NotNull final File file = new File(PathEnum.XML.getPath());
    @NotNull final ObjectMapper objectMapper = new XmlMapper();
    @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
    serviceLocator.getDomainService().load(domain);
    System.out.println("Data load from " + file.getPath());
  }
}

