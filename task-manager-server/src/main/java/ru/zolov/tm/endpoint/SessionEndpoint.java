package ru.zolov.tm.endpoint;

import java.text.ParseException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ISessionEndpoint;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.util.HashUtil;

@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

  @Nullable @Override @WebMethod public Session openSession(
      @NotNull @WebParam(name = "login") String login,
      @NotNull @WebParam(name = "password") String password
  ) throws EmptyStringException, UserNotFoundException, ParseException {
    @Nullable final User user = serviceLocator.getUserService().login(login, password);
    final String passwordHash = HashUtil.md5(password);
    if (passwordHash.equals(user.getPasswordHash())) {
      return serviceLocator.getSessionService().open(user);
    }
    return null;
  }


  @Override @WebMethod public void closeSesson(
      @NotNull @WebParam(name = "session") Session session
  ) throws AccessForbiddenException, CloneNotSupportedException {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getSessionService().close(session);
  }
}
