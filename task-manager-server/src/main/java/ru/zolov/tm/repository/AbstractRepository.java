package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IRepository;
import ru.zolov.tm.entity.AbstractEntity;

abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

  Map<String, E> storage = new LinkedHashMap<>();

  @NotNull public List<E> findAll() {
    return new ArrayList<>(storage.values());
  }

  public E findOne(@NotNull final String id) {
    return storage.get(id);
  }

  public E persist(@NotNull final E entity) {
    storage.put(entity.getId(), entity);
    return entity;
  }

  public void merge(@NotNull final E entity) {
    storage.put(entity.getId(), entity);
  }

  public boolean remove(@NotNull final String id) {
    return storage.remove(id) != null;
  }

  public void removeAll() {
    storage.clear();
  }

  public void load(@NotNull final List<E> list) {
    for (@NotNull final E entity : list) {
      storage.put(entity.getId(), entity);
    }
  }

}
