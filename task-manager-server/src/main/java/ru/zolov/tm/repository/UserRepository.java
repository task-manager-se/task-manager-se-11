package ru.zolov.tm.repository;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.entity.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

  @Override public User findOne(@NotNull final String id) {
    return storage.get(id);
  }

  @NotNull @Override public User persist(@NotNull final User user) {
    storage.put(user.getId(), user);
    return user;
  }

  @Override public void merge(@NotNull final User user) {
    if (storage.containsKey(user.getId())) update(user.getId(), user.getLogin());
    else persist(user);
  }

  @Override public boolean remove(@NotNull final String id) {
    return storage.remove(id) != null;
  }

  @Override public void removeAll() {
    storage.clear();
  }

  @Override public void load(@NotNull List<User> list) {
    for (User user : list) {
      storage.put(user.getId(), user);
    }
  }

  @Override public User findByLogin(@NotNull final String login) {
    User foundedUser = null;
    for (@NotNull final User user : storage.values()) {
      if (user.getLogin().equals(login)) foundedUser = user;
    }
    return foundedUser;
  }

  @Override public void update(
      @NotNull final String id,
      @NotNull final String login
  ) {
    final User user = storage.get(id);
    user.setLogin(login);
  }
}
