package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

  @NotNull @Override public Project persist(
      @NotNull final Project project
  ) {
    storage.put(project.getId(), project);
    return project;
  }

  @Override public void merge(
      @NotNull final Project project
  ) {
    if (storage.containsKey(project.getId()))
      update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateOfStart(), project
          .getDateOfFinish());
    else persist(project);
  }

  @Override public void load(
      @NotNull final List<Project> list
  ) {
    for (@NotNull final Project project : list) {
      storage.put(project.getId(), project);
    }
  }

  @NotNull @Override public Project findOne(
      @NotNull final String userId,
      @NotNull final String id
  ) throws EmptyStringException, EmptyRepositoryException {
    final Project project = storage.get(id);
    if (project == null) {
      throw new EmptyRepositoryException();
    }
    if (!project.getUserId().equals(userId)) {
      throw new EmptyStringException();
    }
    return project;
  }

  @NotNull @Override public List<Project> findAll(
      @NotNull final String userId
  ) throws EmptyRepositoryException {
    final List<Project> projectList = new ArrayList<>();
    for (@Nullable final Project project : storage.values()) {
      if (project == null) throw new EmptyRepositoryException();
      if (project.getUserId().equals(userId)) projectList.add(project);
    }
    return projectList;
  }

  @Override public void update(
      @NotNull final String userId,
      @NotNull final String id,
      @NotNull final String name,
      @NotNull final String description,
      @Nullable final Date start,
      @Nullable final Date finish
  ) {
    final Project project = storage.get(id);
    project.setUserId(userId);
    project.setName(name);
    project.setDescription(description);
    project.setDateOfStart(start);
    project.setDateOfFinish(finish);
  }

  @Override public boolean remove(
      @NotNull final String userId,
      @NotNull final String id
  ) {
    final Project project = storage.get(id);
    if (project.getUserId().equals(userId)) {
      storage.remove(id);
      return true;
    }
    return false;
  }

  @Override public void removeAll(
      @NotNull final String userId
  ) throws EmptyRepositoryException {
    for (Project project : findAll(userId)) {
      storage.remove(project);
    }
  }

  @NotNull @Override public List<Project> findProject(
      @NotNull final String userId,
      @NotNull final String partOfTheName
  ) throws EmptyRepositoryException {
    @NotNull final List<Project> resultList = new ArrayList<>();
    for (@NotNull final Project project : findAll(userId)) {
      if (project.getName().contains(partOfTheName) || project.getDescription().contains(partOfTheName)) {
        resultList.add(project);
      }
    }
    return resultList;
  }
}
